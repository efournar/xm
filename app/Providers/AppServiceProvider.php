<?php

namespace App\Providers;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register() {
    
    	// whenever a class needs an object that implements CompanyInfoApi, a DatahubNasdaq object will be served
		$this->app->bind('App\Contracts\Nasdaq\CompanyInfoApi', 'App\Utilities\DatahubNasdaq');
	
		// whenever a class needs an object that implements StockInfoApi, a RapidStock object will be served
		$this->app->bind('App\Contracts\Nasdaq\StockInfoApi', 'App\Utilities\RapidStock');
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot() {
    
    	// applying default string length for keys in database
		Schema::defaultStringLength(191);
		
    }
}
