<?php

namespace Tests\Unit\Providers;

use App\Utilities\DatahubNasdaq;
use App\Utilities\RapidStock;
use Tests\TestCase;

class AppServiceProviderTest extends TestCase {
	
	public function testRegisterCompanyInfoApiIsImplementedWithDatahubNasdaq() {
		
		$companyInfoImplementorObject = app('App\Contracts\Nasdaq\CompanyInfoApi');
		
		$this->assertInstanceOf(DatahubNasdaq::class, $companyInfoImplementorObject);
		
	}
	
	public function testRegisterStockInfoApiIsImplementedWithRapidStock() {
		
		$stockInfoImplementorObject = app('App\Contracts\Nasdaq\StockInfoApi');
		
		$this->assertInstanceOf(RapidStock::class, $stockInfoImplementorObject);
	
	}
}