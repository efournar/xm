<?php

namespace Tests\Unit\Utilities;

use App\Utilities\DatahubNasdaq;
use App\Utilities\RapidStock;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Tests\TestCase;

class RapidStockTest extends TestCase {
	
	protected $rapidStock;
	
	protected $testClientResponseBody;
	
	protected function setUp(): void {
		parent::setUp();
		
		$this->testClientResponseBody = json_encode([
			'prices' => [
					[
						"date"=>"2020-09-09",
						"open"=>13.479999542236328,
						"high"=>13.579999923706055,
						"low"=>12.800000190734863,
						"close"=>13.050000190734863,
						"volume"=>79030200
					],
					[
						"date"=>"2020-09-08",
						"open"=>13.359999656677246,
						"high"=>14.15999984741211,
						"low"=>13.149999618530273,
						"close"=>13.630000114440918,
						"volume"=>72746000
					],
					[
						"date"=>"2020-09-04",
						"open"=>13.649999618530273,
						"high"=>13.819999694824219,
						"low"=>12.960000038146973,
						"close"=>13.609999656677246,
						"volume"=>64937000
					]
			]
		]);
		
		$mock = new MockHandler([
			new Response(200, ['X-Foo' => 'Bar'], $this->testClientResponseBody),
		]);
		
		$handlerStack = HandlerStack::create($mock);
		
		$client = new Client(['handler' => $handlerStack]);
		
		$this->rapidStock = new RapidStock($client);
	}
	
	public function testGetApiSourceUrl() {
		
		$expectedCacheTimeSeconds = config('rapidstock.api_source_url');
		
		$this->assertSame($expectedCacheTimeSeconds, $this->rapidStock->getApiSourceUrl());
		
	}
	
	public function testGetApiServiceKey() {
		
		$expectedApiServiceKey = config('rapidstock.api_service_key');
		
		$this->assertSame($expectedApiServiceKey, $this->rapidStock->getApiServiceKey());
		
	}
	
	public function testGetClient() {
		
		$this->assertInstanceOf(Client::class, $this->rapidStock->getClient());
	}
	
	public function testGetStockInfoBetweenDatesWithEmptyOneParameter() {
	
		$this->expectException(\Exception::class);
		
		$this->rapidStock->getStockInfoBetweenDates('AAL', null, Carbon::today()->format('Y-m-d'));
		
	}
	
	public function testGetStockInfoBetweenDatesWithErrorInCommunication() {
		
		$this->expectException(\Exception::class);
		
		// create mock handler with response
		$mock = new MockHandler([
			new RequestException('Error Communicating with Server', new Request('GET', 'test'))
		]);
		
		$handlerStack = HandlerStack::create($mock);
		
		// initializating objects
		$client     = new Client(['handler' => $handlerStack]);
		$rapidStock = new RapidStock($client);
		
		// set dates for the request
		$today           = Carbon::today()->format('Y-m-d');
		$beforeThreeDays = Carbon::today()->subDays(3)->format('Y-m-d');
		
		$rapidStock->getStockInfoBetweenDates('AAL', $beforeThreeDays, $today);
		
	}
	
	public function testGetStockInfoBetweenDates() {
	
		// set dates for the request
		$today  = Carbon::parse('2020-09-09')->format('Y-m-d');
		$before = Carbon::parse('2020-09-04')->format('Y-m-d');

		$dataResponse = $this->rapidStock->getStockInfoBetweenDates('AAL', $before, $today);
		
		$dataArray = $dataResponse->all();
		
		$this->assertIsArray($dataArray);
		
		$randomItem = Arr::random($dataArray);

		$this->assertArrayHasKey('date', $randomItem);
		$this->assertArrayHasKey('open', $randomItem);
		$this->assertArrayHasKey('high', $randomItem);
		$this->assertArrayHasKey('low', $randomItem);
		$this->assertArrayHasKey('close', $randomItem);
		$this->assertArrayHasKey('volume', $randomItem);
		
		$this->assertArrayNotHasKey('non_existing_key', $randomItem);
		
	}
	
}