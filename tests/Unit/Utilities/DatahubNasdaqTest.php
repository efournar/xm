<?php

namespace Tests\Unit\Utilities;

use App\Utilities\DatahubNasdaq;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Tests\TestCase;

class DatahubNasdaqTest extends TestCase {
	
	protected $datahubNasdaq;
	
	protected $testClientResponse;
	
	protected function setUp(): void {
		parent::setUp();
		
		$this->testClientResponse = [
			[
				"Company Name"     => "iShares MSCI All Country Asia Information Technology Index Fund",
				"Financial Status" => "N",
				"Market Category"  => "G",
				"Round Lot Size"   => 100.0,
				"Security Name"    => "iShares MSCI All Country Asia Information Technology Index Fund",
				"Symbol"           => "AAIT",
				"Test Issue"       => "N"
			],
			[
				"Company Name"     => "American Airlines Group, Inc.",
				"Financial Status" => "N",
				"Market Category"  => "Q",
				"Round Lot Size"   => 100.0,
				"Security Name"    => "American Airlines Group, Inc. - Common Stock",
				"Symbol"           => "AAL",
				"Test Issue"       => "N"
			],
		];
		
		$mock = new MockHandler([
			new Response(200, ['X-Foo' => 'Bar'], json_encode($this->testClientResponse)),
		]);
		
		$handlerStack = HandlerStack::create($mock);
		
		$client = new Client(['handler' => $handlerStack]);
		
		$this->datahubNasdaq = new DatahubNasdaq($client);
	}
	
	public function testGetCacheKey() {
	
		$expectedCacheKey = config('datahubnasdaq.cache_key', 'nasdal_info');
		
		$this->assertEquals($expectedCacheKey, $this->datahubNasdaq->getCacheKey());
	}
	
	public function testGetCacheTimeSeconds() {
		
		$expectedCacheTimeSeconds = config('datahubnasdaq.cache_time_seconds', 300);
		
		$this->assertEquals($expectedCacheTimeSeconds, $this->datahubNasdaq->getCacheTimeSeconds());
	}
	
	public function testGetApiSourceUrl() {
		
		$expectedCacheTimeSeconds = config('datahubnasdaq.api_source_url');
		
		$this->assertEquals($expectedCacheTimeSeconds, $this->datahubNasdaq->getApiSourceUrl());
		
	}
	
	public function testGetClient() {
		
		$this->assertInstanceOf(Client::class, $this->datahubNasdaq->getClient());
	}
	
	public function testGetData() {
		
		$dataResponse = $this->datahubNasdaq->getData();
		
		$this->assertIsArray($dataResponse);
		
		$this->assertEquals(2, count($dataResponse));
	}
	
	public function testGetSymbols() {
		
		$dataResponse = $this->datahubNasdaq->getSymbols();
		
		$this->assertIsObject($dataResponse);
		
		$data = $dataResponse->all();
		
		$this->assertEquals(2, count($data));
		
		$this->assertEquals('AAIT', reset($data));
	}
	
	public function testGetCompanyNameWithSymbols() {
		$dataResponse = $this->datahubNasdaq->getCompanyNameWithSymbols();
		
		$this->assertIsObject($dataResponse);
		
		$this->assertEquals(2, $dataResponse->count());
		
		$firstItemInArray = $dataResponse->first();
		
		$this->assertEquals(2, count($firstItemInArray));
		$this->assertArrayHasKey('company_name', $firstItemInArray);
		$this->assertArrayNotHasKey('Financial Status', $firstItemInArray);
		$this->assertArrayHasKey('symbol', $firstItemInArray);
	}
	
	public function testGetDataUnsuccessful() {
		
		$this->expectException(\Exception::class);
		
		$mock = new MockHandler([
			new RequestException('Error Communicating with Server', new Request('GET', 'test'))
		]);
		
		$handlerStack = HandlerStack::create($mock);
		
		$client = new Client(['handler' => $handlerStack]);
		
		$datahubNasdaq = new DatahubNasdaq($client);
		
		$datahubNasdaq->getData();
	}
	
}
