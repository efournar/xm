<?php

namespace Tests\Feature\Http\Controllers\Api;

use App\Mail\FormSubmittedMail;
use App\Utilities\DatahubNasdaq;
use App\Utilities\RapidStock;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use Tests\TestCase;

class ApiControllerTest extends TestCase {
	
	public function testGetHistoricalDataWithEmptySymbol() {
		
		// get valid data
		$dataToPost = $this->getValidDataForValidation();
		
		// alter one parameter in order to check validation
		$dataToPost['symbol'] = '';
		
		// call the following route with params
		$response = $this->call('GET', '/api/historical/data', $dataToPost);
		
		// assert that http status is 200
		$response->assertStatus(200);
		
		// check that in response there is error key
		$this->assertArrayHasKey('error', $response);
		
		// check that error in response is not empty
		$this->assertNotEmpty($response['error']);
		
		// check that it contains the phrase in error
		$this->assertStringContainsString('symbol field is required', $response['error']);
		
	}
	
	public function testGetHistoricalDataWithWrongSymbol() {
		
		// get valid data
		$dataToPost = $this->getValidDataForValidation();
		
		// alter one parameter in order to check validation
		$dataToPost['symbol'] = '12G34';
		
		// call the following route with params
		$response = $this->call('GET', '/api/historical/data', $dataToPost);
		
		// assert that http status is 200
		$response->assertStatus(200);
		
		// check that in response there is error key
		$this->assertArrayHasKey('error', $response);
		
		// check that error in response is not empty
		$this->assertNotEmpty($response['error']);
		
		// check that it contains the phrase in error
		$this->assertStringContainsString('symbol is invalid', $response['error']);
		
	}

	public function testGetHistoricalDataWithStartDateEmpty() {
		
		// get valid data
		$dataToPost = $this->getValidDataForValidation();
		
		// alter one parameter in order to check validation
		$dataToPost['startDate'] = '';
		
		/// call the following route with params
		$response = $this->call('GET', '/api/historical/data', $dataToPost);
		
		// assert that http status is 200
		$response->assertStatus(200);
		
		// check that in response there is error key
		$this->assertArrayHasKey('error', $response);
		
		// check that error in response is not empty
		$this->assertNotEmpty($response['error']);
		
		// check that it contains the phrase in error
		$this->assertStringContainsString('start date field is required', $response['error']);
		
	}

	public function testGetHistoricalDataWithInvalidStartDate() {
		
		// get valid data
		$dataToPost = $this->getValidDataForValidation();
		
		// alter one parameter in order to check validation
		// this works equal with endDate
		$dataToPost['startDate'] = '123123123';
		
		/// call the following route with params
		$response = $this->call('GET', '/api/historical/data', $dataToPost);
		
		// assert that http status is 200
		$response->assertStatus(200);
		
		// check that in response there is error key
		$this->assertArrayHasKey('error', $response);
		
		// check that error in response is not empty
		$this->assertNotEmpty($response['error']);
		
		// check that it contains the phrase in error
		$this->assertStringContainsString('start date is not a valid date', $response['error']);
		
	}
	
	public function testGetHistoricalDataWithInvalidEndDate() {
		
		// get valid data
		$dataToPost = $this->getValidDataForValidation();
		
		// alter one parameter in order to check validation
		// this works equal with endDate
		$dataToPost['endDate'] = '9999-99-99';
		
		/// call the following route with params
		$response = $this->call('GET', '/api/historical/data', $dataToPost);
		
		// assert that http status is 200
		$response->assertStatus(200);
		
		// check that in response there is error key
		$this->assertArrayHasKey('error', $response);
		
		// check that error in response is not empty
		$this->assertNotEmpty($response['error']);
		
		// check that it contains the phrase in error
		$this->assertStringContainsString('end date is not a valid date', $response['error']);
		
	}
	

	public function testGetHistoricalDataWithWrongFormatForStartDate() {
		
		// get valid data
		$dataToPost = $this->getValidDataForValidation();
		
		// alter one parameter in order to check validation
		// this works equal with endDate
		$dataToPost['startDate'] = Carbon::today()->subDays(10)->format('d/m/Y');
		
		/// call the following route with params
		$response = $this->call('GET', '/api/historical/data', $dataToPost);
		
		// assert that http status is 200
		$response->assertStatus(200);
		
		// check that in response there is error key
		$this->assertArrayHasKey('error', $response);
		
		// check that error in response is not empty
		$this->assertNotEmpty($response['error']);
		
		// check that it contains the phrase
		$this->assertStringContainsString('start date does not match the format', $response['error']);
		
	}
	
	public function testGetHistoricalDataWithWrongFormatForEndDate() {

		// get valid data
		$dataToPost = $this->getValidDataForValidation();

		// alter one parameter in order to check validation
		// this works equal with endDate
		$dataToPost['endDate'] = Carbon::today()->format('Y/m/d');

		/// call the following route with params
		$response = $this->call('GET', '/api/historical/data', $dataToPost);

		// assert that http status is 200
		$response->assertStatus(200);

		// check that in response there is error key
		$this->assertArrayHasKey('error', $response);

		// check that error in response is not empty
		$this->assertNotEmpty($response['error']);

		// check that it contains the phrase
		$this->assertStringContainsString('end date does not match the format', $response['error']);

	}

	public function testGetHistoricalDataWithStartDateGreaterThanToday() {
		
		// get valid data
		$dataToPost = $this->getValidDataForValidation();
		
		// alter one parameter in order to check validation
		$dataToPost['startDate'] = Carbon::today()->addDays(5)->format('Y-m-d');
		$dataToPost['endDate'] = Carbon::today()->addDays(10)->format('Y-m-d');
		
		/// call the following route with params
		$response = $this->call('GET', '/api/historical/data', $dataToPost);
		
		// assert that http status is 200
		$response->assertStatus(200);
		
		// check that in response there is error key
		$this->assertArrayHasKey('error', $response);
		
		// check that error in response is not empty
		$this->assertNotEmpty($response['error']);
		
		// check that it contains the phrase
		$this->assertStringContainsString('start date must be a date before or equal to today', $response['error']);
		
	}
	
	public function testGetHistoricalDataWithStartDateGreaterThanEndDate() {
		
		// get valid data
		$dataToPost = $this->getValidDataForValidation();
		
		// alter one parameter in order to check validation
		$dataToPost['startDate'] = Carbon::today()->subDays(5)->format('Y-m-d');
		$dataToPost['endDate'] = Carbon::today()->subDays(10)->format('Y-m-d');
		
		/// call the following route with params
		$response = $this->call('GET', '/api/historical/data', $dataToPost);
		
		// assert that http status is 200
		$response->assertStatus(200);
		
		// check that in response there is error key
		$this->assertArrayHasKey('error', $response);
		
		// check that error in response is not empty
		$this->assertNotEmpty($response['error']);
		
		// check that it contains the phrase
		$this->assertStringContainsString(' start date must be a date before or equal to end date', $response['error']);
		
	}

	public function testGetHistoricalDataWithEndDateGreaterThanToday() {
		
		// get valid data
		$dataToPost = $this->getValidDataForValidation();
		
		// alter one parameter in order to check validation
		$dataToPost['endDate'] = Carbon::today()->addDays(10)->format('Y-m-d');
		
		/// call the following route with params
		$response = $this->call('GET', '/api/historical/data', $dataToPost);
		
		// assert that http status is 200
		$response->assertStatus(200);
		
		// check that in response there is error key
		$this->assertArrayHasKey('error', $response);
		
		// check that error in response is not empty
		$this->assertNotEmpty($response['error']);
		
		// check that it contains the phrase
		$this->assertStringContainsString('end date must be a date before or equal to today', $response['error']);
		
	}

	public function testGetHistoricalDataWithEmailEmpty() {
		
		// get valid data
		$dataToPost = $this->getValidDataForValidation();
		
		// alter one parameter in order to check validation
		$dataToPost['email'] = '';
		
		/// call the following route with params
		$response = $this->call('GET', '/api/historical/data', $dataToPost);
		
		// assert that http status is 200
		$response->assertStatus(200);
		
		// check that in response there is error key
		$this->assertArrayHasKey('error', $response);
		
		// check that error in response is not empty
		$this->assertNotEmpty($response['error']);
		
		// check that it contains the phrase
		$this->assertStringContainsString('email field is required', $response['error']);
		
	}
	
	public function testGetHistoricalDataWithInvalidEmail() {
		
		// get valid data
		$dataToPost = $this->getValidDataForValidation();
		
		// alter one parameter in order to check validation
		$dataToPost['email'] = 'eliasgearsoft.gr';
		
		/// call the following route with params
		$response = $this->call('GET', '/api/historical/data', $dataToPost);
		
		// assert that http status is 200
		$response->assertStatus(200);
		
		// check that in response there is error key
		$this->assertArrayHasKey('error', $response);
		
		// check that error in response is not empty
		$this->assertNotEmpty($response['error']);
		
		// check that it contains the phrase
		$this->assertStringContainsString('email must be a valid email address', $response['error']);
		
	}

	public function testGetHistoricalDataWithValidData() {
		Mail::fake();
		
		// Assert that no mailables were sent...
		Mail::assertNothingSent();
		Mail::assertNothingQueued();
		
		// get valid data
		$dataToSend = $this->getValidDataForValidation();
		
		// call the following route with params
		$response = $this->call('GET', '/api/historical/data', $dataToSend);
		
		// assert that http status is 200
		$response->assertStatus(200);
		
		// Assert a specific type of mailable was dispatched meeting the given truth test...
		Mail::assertQueued(FormSubmittedMail::class, function ($mail) use ($dataToSend) {
			$m = $mail->build();
			
			$body = view($m->view, $m->buildViewData())->render();
			
			$this->assertEquals($mail->companyName, $mail->subject);
			$this->assertEquals($dataToSend['startDate'], $mail->startDate);
			$this->assertEquals($dataToSend['endDate'], $mail->endDate);
			$this->assertEquals($dataToSend['email'], $mail->email);
			$this->assertEquals('From '.$dataToSend['startDate'].' to '.$dataToSend['endDate'], $body);
			
			return true;
		});
		
		// check that in response there is status key
		$this->assertArrayHasKey('status', $response);
		
		// check that in response that status is TRUE
		$this->assertTrue($response['status']);
		
		// check that in response there is error key
		$this->assertArrayHasKey('error', $response);
		
		// check that error in response is empty
		$this->assertEmpty($response['error']);
		
		// check that in response there is data key
		$this->assertArrayHasKey('data', $response);
		
		// check that there are data in response
		$this->assertGreaterThan(0, count($response['data']));
	}
	
	public function testOtherActionReturns404() {
		
		// calling following route
		$response = $this->get('/api/something/random');
		
		// assert that http status is 404
		$response->assertStatus(404);
		
	}
	
	protected function getValidDataForValidation() {
		return [
			'symbol'    => 'AAL',
			'startDate' => Carbon::today()->subDays(20)->format('Y-m-d'),
		    'endDate'   => Carbon::today()->format('Y-m-d'),
			'email'     => 'elias@gearsoft.gr',
		];
	}
}