<?php

namespace Tests\Feature\Http\Controllers\View;

use Tests\TestCase;

class HomeControllerTest extends TestCase {

	public function testIndexActionLoads() {
	
		// calling following route
		$response = $this->get('/');
		
		// assert that http status is 200
		$response->assertStatus(200);
	
	}
	
	public function testOtherActionReturns404() {
		
		// calling following route
		$response = $this->get('/something/random');
		
		// assert that http status is 200
		$response->assertStatus(404);
		
	}
}