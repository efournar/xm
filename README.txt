
Instructions
--------------------------------

1. Download source code throught git clone. It will create a folder with name 'xm' and inside will download all files.

git clone https://efournar@bitbucket.org/efournar/xm.git


2. Create an '.env' file. This file has custom settings (databases, mail server settings etc) and sensitive settings(passwords) for the project. I supply you with a file like this, but you need to change database, mail settings and RAPID_API_SERVICE_KEY.

3. Run the following command to download php dependencies

composer install

4. Run the following command to download js dependencies

npm install

5. Run the following command to create the appropriate tables in your database

php artisan migrate

6. Run the following command to compile js/css assets

npm run dev

7. Run the following command to start a thread that watches the job queue. We use queue for sending emails, in order not to
delay the execution time for response of the 'submit-form' request. It is required in order to send email.

 php artisan queue:listen


8. Start webserver in an other terminal

php artisan serve


9. Open your browser and navigate to the url that is provided, e.g. http://127.0.0.1:8000



For any questions feel free to ask me