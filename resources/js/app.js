// importing libraries

import Vue from 'vue';
import VueResource from 'vue-resource'
var _ = require('lodash');


// configuring vue

Vue.config.productionTip = false;
Vue.config.devtools = true;
Vue.use(VueResource);

Vue.prototype._ = window._;

//register component

var BasicForm = Vue.component('basic-form', require('./components/BasicForm.vue').default);

// initializing vue
var app = new Vue({
    el: '#app'
})


// setting jquery-ui initialiazation after dom is rendered
$(document).ready(function(){

    var dateFormat = "yy-mm-dd";

    var startDate = $('#start_date').datepicker({
        dateFormat: dateFormat,
        maxDate: 0,
        onSelect: function(dateText) {
            // dispatching change event for input, in order to vue to handle it
            $(this)[0].dispatchEvent(new Event('input', { 'bubbles': true }))

            // the value of start date is the minimum value for end date
            endDate.datepicker( "option", "minDate", getDate( this ) );
        }
    });

    var endDate = $('#end_date').datepicker({
        dateFormat: dateFormat,
        maxDate: 0,
        onSelect: function(dateText) {
            // dispatching change event for input, in order to vue to handle it
            $(this)[0].dispatchEvent(new Event('input', { 'bubbles': true }))

            // the value of end date is the max value for start date
            startDate.datepicker( "option", "maxDate", getDate( this ) );
        }
    });

    // Get value of date jquery-ui element
    function getDate( element ) {
        var date;
        try {
            date = $.datepicker.parseDate( dateFormat, element.value );
        } catch( error ) {
            date = null;
        }
        return date;
    }

});
