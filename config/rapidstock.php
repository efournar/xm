<?php

return [
	
	// Url where Nasdaq Stock Historical data can be found
	'api_source_url' => 'https://apidojo-yahoo-finance-v1.p.rapidapi.com/stock/v2/get-historical-data',

	// Api service key for requests
	'api_service_key' => env('RAPID_API_SERVICE_KEY', ''),
];